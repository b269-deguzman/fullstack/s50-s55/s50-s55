import {Card, Button, Row, Col} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom'

export default function CourseCard({course}){

	// Deconstruct the course properties into their own variables
	const {name, description, price, _id} = course;

	/*
		SYNTAX:
			const [getter, setter] = useState(initialGetterValue)
	*/
// 	const [count, setCount] = useState(0);
// 	const [seats, setSeats] = useState(5);

// 	function enroll(){
// 			setCount(count + 1)
// 			setSeats(seats -1)
// 		}

// useEffect(()=>{
// 	if(seats <=0){
// 		alert("No more seats available")
// 	}
// }, [seats])

// // S51 [ACTIVITY]
// 	const [seats, setSeats] = useState(30);
// 	function availSeats(){
// 		if(seats >0){
// 			enroll()
// 			setSeats(seats-1)
// 		}else{
// 			alert(`No more seats.`)
// 		}
// 	}

	return(
		<Row className="mt-3 mb-3">
		    <Col xs={12}>
		        <Card className="cardHighlight p-0">
		            <Card.Body>
		                <Card.Title><h4>{name}</h4></Card.Title>
		                <Card.Subtitle>Description</Card.Subtitle>
		                <Card.Text>{description}</Card.Text>
		                <Card.Subtitle>Price</Card.Subtitle>
		                <Card.Text>{price}</Card.Text>
		                 {/*<Card.Subtitle>{count} Enrollees</Card.Subtitle>
		                 <Card.Subtitle>{seats} Seats</Card.Subtitle>*/}
		                {/*<Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>*/}

		               	<Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
		            </Card.Body>
		        </Card>
		    </Col>
		</Row>       
	)
}
	// <Row className="mt-3 mb-3">
	// 	<Col xs={12}>
	// 		<Card className="cardHighlight p-0">
	// 			<Card.Body>
	// 				<Card.Title><h4>Front-End</h4></Card.Title>
	// 				<Card.Text><h6>Description:</h6></Card.Text>
	// 				<Card.Text>This course offers HTML CSS and Bootstrap!</Card.Text>
	// 				<Card.Text><h6>Price:</h6></Card.Text>
	// 				<Card.Text>PHP 10,000</Card.Text>
	// 				<Button variant="primary">Enroll</Button>
	// 			</Card.Body>
	// 		</Card>
	// 	</Col>
	// </Row>
