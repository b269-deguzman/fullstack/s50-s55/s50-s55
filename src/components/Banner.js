// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

import { Button, Row, Col} from 'react-bootstrap';
import {Link, useLocation} from 'react-router-dom'

export default function Banner({bannerData}) {
const location = useLocation();

return (
    <Row>
    	<Col className="p-5">
            <h1>{bannerData.header}</h1>
            <p>{bannerData.paragraph}</p>
            {location.pathname==="/"? <Button variant="primary" as={Link} to='/courses'>{bannerData.button}</Button> : <Button variant="primary" as={Link} to='/'>{bannerData.button}</Button> }
            
        </Col>
    </Row>
	)
}