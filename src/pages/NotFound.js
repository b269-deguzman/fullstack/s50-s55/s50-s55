import Banner from '../components/Banner'
import bannerData from '../data/bannerData'

export default function NotFound(){
	return(
		<>
		<Banner bannerData = {bannerData[1]}/>
		</>
	)
}