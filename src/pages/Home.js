import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import bannerData from '../data/bannerData'


export default function Home(){
	return (
		<>
			< Banner bannerData = {bannerData[0]}/>
			< Highlights/>
		</>
	)
}