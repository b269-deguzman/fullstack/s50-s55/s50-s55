const bannerData = [
{
	header: "Zuitt Coding Bootcamp",
	paragraph: "Opportunities for everyone, everywhere.",
	button: "Enroll Now"
},
{
	header: "Error 404 - Page not found",
	paragraph: "The page you are looking for cannot be found",
	button: "Back To Home"
}
]

export default bannerData;